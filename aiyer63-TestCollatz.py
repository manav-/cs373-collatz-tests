#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, collatz_stop_time, fill_cache

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read2(self):
        s = "1009                     12\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1009)
        self.assertEqual(j, 12)

    def test_read_3(self):
        s = "\t12        \t\t\t 21\n\n\n\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 12)
        self.assertEqual(j, 21)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(10, 1)
        self.assertEqual(v, 20)

    def test_eval_6(self):
        v = collatz_eval(1, 17)
        self.assertEqual(v, 20)

    def test_eval_7(self):
        v = collatz_eval(18, 1)
        self.assertEqual(v, 21)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, -1, 123, 525)
        self.assertEqual(w.getvalue(), "-1 123 525\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 12, 1000000, 1290)
        self.assertEqual(w.getvalue(), "12 1000000 1290\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("\n1 100\n1 17\n18 1\n1 100\n1000 1\n\n\n\n\n\n\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 100 119\n1 17 20\n18 1 21\n1 100 119\n1000 1 179\n")

    # ---------
    # stop_time
    # ---------

    def test_stop_time_1(self):
        v = collatz_stop_time(9)
        self.assertEqual(v, 20)

    def test_stop_time_2(self):
        v = collatz_stop_time(834215)
        self.assertEqual(v, 189)

    def test_stop_time_3(self):
        v = collatz_stop_time(1)
        self.assertEqual(v, 1)

    def test_stop_time_4(self):
        v = collatz_stop_time(2)
        self.assertEqual(v, 2)

    def test_stop_time_5(self):
        v = collatz_stop_time(18)
        self.assertEqual(v, 21)

    # ----------
    # fill cache
    # ----------

    def test_fill_cache_1(self):
        length = 100
        expected = [100, 99, 97, 96, 94, 93, 91, 90, 88, 87]
        cache = [0] * 10
        nums = list(range(10))
        res = fill_cache(nums, length, cache)
        self.assertListEqual(cache, expected)

    def test_fill_cache_2(self):
        length = 100
        nums = [3, 10, 5, 16, 8, 4, 2, 1]
        expected = [0, 91, 92, 100, 93, 97, 0, 0, 94, 0, 98, 0, 0, 0, 0, 0, 95]
        cache = [0] * 17
        res = fill_cache(nums, length, cache)
        self.assertListEqual(cache, expected)

    def test_fill_cache_3(self):
        length = 100
        nums = [1, 2, 1, 2, 1, 2, 1, 2, 1]
        expected = [0, 88, 89]
        cache = [0, 12, 14]
        res = fill_cache(nums, length, cache)
        self.assertListEqual(cache, expected)

# ----
# main
# ----


if __name__ == "__main__":
    main()


""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
