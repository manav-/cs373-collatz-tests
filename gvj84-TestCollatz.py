#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import SEEK_CUR, StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, cycle_length, collatz_range

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    # Should an input string containing more than 2 numbers fail?
    #TODO Ask on piazza
    def test_read_2(self):
        s = "2 42 5"
        i, j = collatz_read(s)
        self.assertEqual(i, 2)
        self.assertEqual(j, 42)

        self.assertNotEqual(i, 5)
        self.assertNotEqual(j, 5)

    def test_read_3(self):
        s = "34 56\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  34)
        self.assertEqual(j, 56)
        
    # ----
    # eval
    # ----

    # Updating Tests
    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        l = [1, 2, 3]
        collatz_print(w, 0, 0, l)
        self.assertEqual(w.getvalue(), "0 0 " + str(l) + "\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, None, None, None)
        self.assertEqual(w.getvalue(), "None None None\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("3 45\n46 12\n50 120\n75 47\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "3 45 112\n46 12 112\n50 120 119\n75 47 116\n")
        
    def test_solve_3(self):
        r = StringIO("1 1\n10 1\n200 125\n8 8\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1 1\n10 1 20\n200 125 125\n8 8 4\n")

    """
    Helper method testers specific to our code
    # ----
    # cycle length
    # ----
    def test_cycle_length_1(self):
        actual = cycle_length(1)
        expected = 1
        self.assertEqual(actual, expected)

    def test_cycle_length_2(self):
        actual = cycle_length(10)
        expected = 7
        self.assertEqual(actual, expected)
        
    def test_cycle_length_3(self):
        actual = cycle_length(5)
        expected = 6
        self.assertEqual(actual, expected)

    # ----
    # collatz_range
    # ----
    def test_collatz_range_1(self):
        actual = collatz_range(1, 2)
        expected = 8
        self.assertEqual(actual, expected)

    def test_collatz_range_2(self):
        actual = collatz_range(4, 6)
        expected = 9
        self.assertEqual(actual, expected)

    def test_collatz_range_3(self):
        actual = collatz_range(1, 4)
        expected = 8
        self.assertEqual(actual, expected)
    """
# ----
# main
# ----

if __name__ == "__main__":
    main()



""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
